import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Profile from './containers/ProfileContainer'
import Gallery from './containers/GalleryContainer'
import Products from './containers/ProductsContainer'

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={Profile} />
      <Route path="/profile" component={Profile} />
      <Route path="/gallery" component={Gallery} />
      <Route path="/products" component={Products} />
    </Switch>
  )
}
