import firebase from 'firebase/app'
import 'firebase/storage'

var firebaseConfig = {
  apiKey: "AIzaSyBPIzNS_zCAwbZKGsPrp2VTw7eO1o9HKJU",
  authDomain: "test-react-3908c.firebaseapp.com",
  databaseURL: "https://test-react-3908c.firebaseio.com",
  projectId: "test-react-3908c",
  storageBucket: "test-react-3908c.appspot.com",
  messagingSenderId: "65707493291",
  appId: "1:65707493291:web:d8f6930377c48d8014f4ee"
};

firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();

export {
  storage,
  firebase as default
}
