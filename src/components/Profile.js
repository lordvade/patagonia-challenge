import React from 'react'
import './Profile.css';
import { Container, Row, Col, Card, ListGroup, ListGroupItem } from 'react-bootstrap';

const Profile = ({
  profile
}) => {
  return (
    <Container>
      <br />
      <Row>
        <Col md={3}>
          <div className='border-circle' style={{ backgroundImage: `url(${profile.photo})`}} ></div>
        </Col>
        <Col md={9}>
          <Card>
            <Card.Body>
              <Card.Title>{profile.name}</Card.Title>
              <Card.Text>
                {profile.description}
              </Card.Text>
            </Card.Body>
            <ListGroup className="list-group-flush">
              <ListGroupItem></ListGroupItem>
              <ListGroupItem>{profile.email}</ListGroupItem>
              <ListGroupItem>{profile.phone}</ListGroupItem>
              <ListGroupItem>{profile.enterprise}</ListGroupItem>
            </ListGroup>
            <Card.Body>
              <Card.Link href={profile.linkedin}>Linkedin</Card.Link>
              <Card.Link href={profile.github}>Github</Card.Link>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

export default Profile;
