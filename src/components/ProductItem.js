import React from 'react'

const ProductItem = ({ product }) => {
  return  <tr>
            <td>{ product.number }</td>
            <td>{ product.name }</td>
            <td>{ product.date }</td>
            <td>{ product.sku }</td>
            <td>{ product.weight }</td>
            <td>{ product.height }</td>
            <td>{ product.width }</td>
            <td>{ product.origin }</td>
            <td>{ product.minimum }</td>
            <td>{ product.delay }</td>
          </tr>
}

export default ProductItem;
