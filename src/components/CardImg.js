import React from 'react'
import { Card } from 'react-bootstrap'
import { Button } from 'react-bootstrap'

const CardImg = ({ img, deleteImg }) => (
  <Card className="p-3">
    <Card.Img variant="top" src={img.Url} alt={img.name} />
    <Button variant="danger" onClick={() => { deleteImg(img.name) }} >Delete</Button>
  </Card>
)

export default CardImg;
