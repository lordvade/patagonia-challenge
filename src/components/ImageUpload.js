import React, { useState, useEffect } from 'react'
import { storage } from '../firebase'
import CardImg from './CardImg';
import { CardColumns, Button, ProgressBar } from 'react-bootstrap'

const ImageUpload = () => {
  const [imageAsFile, setImageAsFile] = useState('')
  const [imageAsUrl, setImageAsUrl] = useState([])
  const [percentage, setPercentage] = useState(0)
  const desertRef = storage.ref(`images`)

  const handleImageAsFile = (e) => {
    const image = e.target.files[0]
    setImageAsFile(imageFile => (image))
  }

  const hangleUpload = (e) => {
    e.preventDefault()

    const uploadTask = storage.ref(`images/${imageAsFile.name}`).put(imageAsFile)
    uploadTask.on('state_changed',
      (snapShot) => {
        setPercentage((snapShot.bytesTransferred / snapShot.totalBytes) * 100)
      },
      (error) => {
        console.log(error)
      },
      () => {
        getAllImages()
        setPercentage(0)
      });
  }

  useEffect(() => {
    getAllImages()
  }, []);

  const getAllImages = () => {
    setImageAsUrl([])
    desertRef.listAll().then(function(res){
      res.items.forEach(function(itemRef) {
        storage.ref('images').child(itemRef.name).getDownloadURL()
          .then(fireBaseUrl => {
            setImageAsUrl(prevState =>
              [
                ...prevState, 
                { Url: fireBaseUrl, name: itemRef.name }
              ]
            )
          })
      });
    })
  }

  const deleteImg = (fileName) => {
    desertRef.child(fileName).delete().then(function() {
      getAllImages();
    })
  }

  return  <div>
            { percentage !== 0 && <ProgressBar now={percentage} /> }
            <p>Step 1. Choose imagen</p>
            <p>Step 2. press upload button</p>
            <input type="file" onChange={handleImageAsFile} />
            <Button onClick={hangleUpload} variant="primary">Upload</Button>
            <hr />
            <CardColumns>
              {
                imageAsUrl.map((img, i) => {
                  return <CardImg key={i} img={img} deleteImg={deleteImg} />
                })
              }
            </CardColumns>
          </div>
}

export default ImageUpload;
