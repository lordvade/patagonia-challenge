import React from 'react'
import { Container, Table } from 'react-bootstrap'
import ProductItem from './ProductItem'

const Products = ({ products }) => {
  return (
    <Container>
      <Table responsive="sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Date</th>
            <th>Sku</th>
            <th>Weight</th>
            <th>Height</th>
            <th>width</th>
            <th>Origin</th>
            <th>Minimum</th>
            <th>Delay</th>
          </tr>
        </thead>
        <tbody>
          {
            products.map((product) => {
              return <ProductItem key={product.number} product={product} />
            })
          }
        </tbody>
      </Table>
    </Container>
  )
}

export default Products;
