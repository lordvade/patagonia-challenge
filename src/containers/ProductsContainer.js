import React from 'react'
import productsData from '../data/products.json'
import Products from '../components/Products'

const ProductsContainer = () => (
  <Products products={productsData} />
)

export default ProductsContainer;
