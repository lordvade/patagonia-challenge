import React from 'react'
import profileData from '../data/profile.json'
import Profile from '../components/Profile'

const ProfileContainer = () => (
  <Profile profile={profileData} />
)

export default ProfileContainer;
