import React from 'react'
import Gallery from '../components/Gallery'

const GalleryContainer = () => (
  <Gallery />
)

export default GalleryContainer;
