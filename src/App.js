import React from 'react';
import './App.css';
import Routes from './Routes';
import { Navbar } from 'react-bootstrap';
import {
  Link
} from "react-router-dom";

function App() {
  return (
    <div>
      <Navbar bg="light">
        <Link className="navbar-brand" to="/profile">Profile</Link>
        <Link className="navbar-brand" to="/gallery">Gallery</Link>
        <Link className="navbar-brand" to="/products">Products</Link>
      </Navbar>
      <Routes />
    </div>
  );
}

export default App;
